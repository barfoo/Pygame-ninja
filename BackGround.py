import pygame


class BackGround(object):

    def __init__(self):
        self.image = pygame.image.load('images/background.png')
        self.rect = self.image.get_rect()

    def draw(self, screen):
        screen.blit(self.image, self.rect)