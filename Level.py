from math import atan2, pi
import Wall
import Shuriken
import pygame
import BackGround
import Enemy
import random
import Item
import Shoot
#Defines level and some methods form moving scene


class Level():

    def __init__(self, screen, mapa):
        self.background = BackGround.BackGround()
        self.walls = []
        self.enemies = []
        self.items = []
        self.surface = pygame.Surface((2500, 2500))
        self.cuadrado = pygame.Surface((16, 16))
        self.rect = pygame.Rect(32, 32, 32, 32)
        self.cuadradorect = pygame.Rect(0, 0, 16, 16)
        self.map = [], []
        file_descriptor = open(mapa)
        self.lines = file_descriptor.readlines()
        self.map = [[None] * len(self.lines[0])
             for j in range(len(self.lines))]
        file_descriptor.close()
        for c in range(len(self.lines[0])):
            for r in range(len(self.lines) - 1):
                self.map[r][c] = self.lines[r][c]

    def drawEscene(self):
        self.surface.blit(self.background.image,
            (self.rect.x, self.rect.y, 800, 600))
        for c in range(len(self.lines[0])):
            for r in range(len(self.lines)):
                if self.map[r][c] == "W":
                    x = r * 32
                    y = c * 32
                    wall = Wall.Wall(x, y)
                    self.walls.append(wall)
                if self.map[r][c] == "E":
                    x = r * 32
                    y = c * 32
                    enemy = Enemy.Enemy(y, x)
                    self.enemies.append(enemy)
                if self.map[r][c] == "I":
                    x = r * 32
                    y = c * 32
                    item = Item.Item(x, y)
                    self.items.append(item)
        for wall in self.walls:
            self.surface.blit(wall.image,
                 (wall.rect.x, wall.rect.y, 32, 32))

    def move(self, key, player, shurikens, mouse):
        dx = 0
        dy = 0
        player.animate(key)
        if key[pygame.K_w]:
            dy -= 2
        if key[pygame.K_s]:
            dy += 2
        if key[pygame.K_a]:
            dx -= 2
            player.animate(key)
        if key[pygame.K_d]:
            dx += 2
            player.animate(key)
        #if key[pygame.K_SPACE] and direction != "":
        # Move each axis separately. that this checks for collisions both times.
        if dx != 0:
            self.move_single_axis(dx, 0, player)
        if dy != 0:
            self.move_single_axis(0, dy, player)

    def move_single_axis(self, dx, dy, player):
        self.rect.x -= dx
        self.rect.y -= dy
        x = abs(self.rect.x - player.rect.x)
        y = abs(self.rect.y - player.rect.y)
        for wall in self.walls:
            if wall.rect.colliderect(x + 8, y + 8, 16, 16):
                if dx > 0:
                # Moving right; Hit the left side of the wall
                    self.rect.right += 2
                if dx < 0:
                # Moving left; Hit the right side of the wall
                    self.rect.left -= 2
                if dy > 0:
                # Moving down; Hit the top side of the wall
                    self.rect.bottom += 2
                if dy < 0:
                # Moving up; Hit the bottom side of the wall
                    self.rect.top -= 2

    def update(self, shurikens, player, score, shoots):
        score = score
        x = abs(self.rect.x - player.rect.x)
        y = abs(self.rect.y - player.rect.y)
        for enemy in self.enemies:
            if enemy.rect.colliderect(x, y, 32, 32):
                if enemy.dead is False:
                    player.life -= 1
        for shuriken in shurikens:
            for wall in self.walls:
                if wall.rect.colliderect(shuriken.rect.x,
                        shuriken.rect.y, 8, 8):
                        '''shoot = Shoot.Shoot(shuriken.rect.x,
                               shuriken.rect.y, shuriken.angle + 90)
                        shoots.append(shoot)'''
                        try:
                            shurikens.remove(shuriken)
                        except:
                            continue

        for shoot in shoots:
            for wall in self.walls:
                if wall.rect.colliderect(shoot.rect.x,
                        shoot.rect.y, 8, 8):
                    try:
                        shoots.remove(shoot)
                        '''shootnew = Shoot.Shoot(shoot.rect.x,
                           shoot.rect.y, shoot.angle + 91)
                        shoots.append(shootnew)'''
                    except:
                        continue

        for shoot in shoots:
            for enemy in self.enemies:
                if shoot.rect.colliderect(enemy.rect.x,
                    enemy.rect.y, 32, 32):
                        if enemy.dead is False:
                            score -= 1
                            killsound = pygame.mixer.Sound(
                                'sound/Cancel8-Bit.ogg')
                            killsound.set_volume(0.1)
                            killsound.play()
                            enemy.dead = True

                        elif enemy.dead is True:
                            crossound = pygame.mixer.Sound(
                                'sound/SmallExplosion8-Bit.ogg')
                            crossound.set_volume(0.1)
                            crossound.play()
                            #want to revive dead os hit uncommnet
                            #enemy.dead = False

                        enemy.image = pygame.image.load(
                                'images/zombie_cross.png')

        for shuriken in shurikens:
            for enemy in self.enemies:
                if shuriken.rect.colliderect(enemy.rect.x,
                    enemy.rect.y, 32, 32):
                        if enemy.dead is False:
                            score -= 1
                            killsound = pygame.mixer.Sound(
                                'sound/Cancel8-Bit.ogg')
                            killsound.set_volume(0.1)
                            killsound.play()
                            enemy.dead = True
                            try:
                                shurikens.remove(shuriken)
                            except:
                                continue

                        elif enemy.dead is True:
                            crossound = pygame.mixer.Sound(
                                'sound/SmallExplosion8-Bit.ogg')
                            crossound.set_volume(0.1)
                            crossound.play()
                            #want to revive dead os hit uncommnet
                            #enemy.dead = False
                            try:
                                shurikens.remove(shuriken)
                            except:
                                continue
                        enemy.image = pygame.image.load(
                                'images/zombie_cross.png')

        return score

    def create_shuriken(self, player, shurikens, mouse):
        a = self.rect.x - player.rect.x
        b = self.rect.y - player.rect.y
        x = abs(a)
        y = abs(b)
        angle = self.calc_angle(player.rect.x,
             player.rect.y, mouse[0], mouse[1])
        if(len(shurikens) < 5):
                    shuriken = Shuriken.Shuriken(x, y, angle)
                    shurikens.append(shuriken)

    def randir(self):
        alea = random.randrange(1, 4)
        if alea / 2 > 1:
            direction = "E"
        else:
            direction = "S"
        return direction

    def calc_angle(self, x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        rads = atan2(dx, dy) * 180 / pi + 180
        return rads

