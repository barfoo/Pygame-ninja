import math

import pygame


class Shuriken():

    def __init__(self, x, y, angle):
        #super(Shuriken, self).__init__()
        self.dx = 0
        self.dy = 0
        self.angle = angle
        self.images = []
        #self.images.append(pygame.image.load('images/shurikenempty.png'))
        self.images.append(pygame.image.load('images/shuriken.png'))
        #self.images.append(pygame.image.load('images/shurikenempty.png'))
        self.images.append(pygame.image.load('images/shuriken2.png'))
        #self.images.append(pygame.image.load('images/shurikenempty.png'))
        self.image = pygame.image.load('images/shuriken.png')
        self.rect = pygame.Rect((x, y, 8, 8))
        self.index = 0
        self.rect.x += 12
        self.rect.y += 8
        self.velocity = 0.2
        self.surface = pygame.Surface((8, 8))
        #self.surface.fill(self.image, (self.rect.x, self.rect.y, 16, 16))

    def update(self):
        alfa1 = math.cos(
                self.degrees_to_radians(self.angle))
        alfa2 = math.sin(
                self.degrees_to_radians(self.angle))
        self.dx -= alfa2
        self.dy -= alfa1
        self.rect.x += self.dx * self.velocity
        self.rect.y += self.dy * self.velocity
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
            self.image = self.images[self.index]

    def degrees_to_radians(self, degrees):
        degs = degrees * (math.pi / 180.0)

        return degs

