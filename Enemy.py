import pygame
import random
import math
from math import atan2, pi


class Enemy():

    def __init__(self, x, y):
        #super(Enemy, self).__init__()
        self.dx = 0
        self.dy = 0
        self.images = []
        #self.images.append(pygame.image.load('images/zombie_left.png'))
        self.images.append(pygame.image.load('images/reno_left.png'))
        self.images.append(pygame.image.load('images/reno_left2.png'))
        #self.images.append(pygame.image.load('images/zombie_down.png'))
        #self.images.append(pygame.image.load('images/zombie_up.png'))
        #self.images.append(pygame.image.load('images/zombie_right.png'))
        #self.image = pygame.image.load('images/zombie_down.png')
        self.image = pygame.image.load('images/reno_left.png')
        self.rect = pygame.Rect((x, y, 32, 32))
        self.index = 0
        self.rect.x = x
        self.point_x = x
        self.point_y = y
        self.rect.y = y
        self.angle = 0
        self.velocity = 0.01
        self.dead = False
        self.choque = False
        self.surface = pygame.Surface((48, 48))
        #self.surface.fill(self.image, (self.rect.x, self.rect.y, 16, 16))
        self.direction = self.randirES()

    def update(self, level, player):
        x = abs(self.rect.x - abs(level.rect.x - player.rect.x))
        y = abs(self.rect.y - abs(level.rect.y - player.rect.y))
        if abs(x) < 800 and abs(y) < 600:
            if self.dead is False:
                    self.animate()
                    self.move()
                #if distancex < 200 and distancey < 200:
                    for wall in level.walls:
                        if wall.rect.colliderect(self.rect.x,
                           self.rect.y, 48, 48):
                            self.choque = True
                            if self.dx > 0:
                            # Moving right; Hit the left side of the wall
                                #self.angle += 90
                                self.direction = "E"
                                self.rect.right += 8
                            #self.direction = "E"
                            if self.dx < 0:
                            # Moving left; Hit the right side of the wall
                                #self.angle += 90
                                self.direction = "W"
                                self.rect.left -= 8
                            if self.dy > 0:
                            # if hit going top then change to south dir
                                #self.angle += 90
                                self.direction = "S"
                                self.rect.bottom += 8
                            if self.dy < 0:
                            #if self.direction == "S":
                                self.direction = "N"
                                self.rect.top -= 8

    def move(self):
        if self.direction == "W":
            self.dx += self.velocity
        if self.direction == "E":
            self.dx -= self.velocity
        if self.direction == "N":
            self.dy += self.velocity
        if self.direction == "S":
            self.dy -= self.velocity
        self.rect.x -= self.dx
        self.rect.y -= self.dy

    def animate(self):
        self.index += 1
        if self.index >= len(self.images):
                self.index = 0
        self.image = self.images[self.index]

    def randirES(self):
        alea = random.randrange(1, 4)
        if alea / 2 > 1:
            direction = "E"
        else:
            direction = "S"
        return direction

    def randomize(self):
        alea = random.randrange(1, 25)
        return alea

    def randir(self):
        direction = ""
        alea = random.randrange(1, 4)
        if alea == 1:
            direction = "N"
        if alea == 2:
            direction = "S"
        if alea == 3:
            direction == "E"
        if alea == 4:
            direction == "W"

    def invert_dir(self):
        if self.direction == "N":
            self.direction = "S"
        if self.direction == "S":
            self.direction = "N"
        if self.direction == "E":
            self.direction = "W"
        if self.direction == "W":
            self.direction = "E"

    def rotate(self):
        if self.direction == "N":
            self.direction = "E"
        if self.direction == "S":
            self.direction = "W"
        if self.direction == "E":
            self.direction = "S"
        if self.direction == "W":
            self.direction = "N"
        self.choque = False

    def calc_angle(self, x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        rads = atan2(dx, dy) * 180 / pi + 180
        return rads

    def degrees_to_radians(self, degrees):
        degs = degrees * (math.pi / 180.0)
        return degs