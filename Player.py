import pygame


class Player(object):

    def __init__(self):
        self.timer = 0.05
        self.index = 0
        self.images = []
        self.images_right = []
        self.images_left = []
        self.images_down = []
        self.images_up = []
        self.life = 100
        self.images_right.append(pygame.image.load('images/ninja_right.png'))
        self.images_right.append(pygame.image.load('images/ninja_right2.png'))
        self.images_right.append(pygame.image.load('images/ninja_right3.png'))
        self.images_right.append(pygame.image.load('images/ninja_right.png'))
        self.images_left.append(pygame.image.load('images/ninja_left.png'))
        self.images_left.append(pygame.image.load('images/ninja_left2.png'))
        self.images_left.append(pygame.image.load('images/ninja_left3.png'))
        self.images_left.append(pygame.image.load('images/ninja_left.png'))
        self.images_down.append(pygame.image.load('images/ninja_down.png'))
        self.images_down.append(pygame.image.load('images/ninja_down2.png'))
        self.images_down.append(pygame.image.load('images/ninja_down3.png'))
        self.images_up.append(pygame.image.load('images/ninja_up.png'))
        self.images_up.append(pygame.image.load('images/ninja_up2.png'))
        self.images_up.append(pygame.image.load('images/ninja_up3.png'))
        self.image = self.images_down[self.index]
        self.images = self.images_down
        self.rect = pygame.Rect(512, 384, 32, 32)
        self.surface = pygame.Surface((16, 16))

    def animate(self, key):
        if key[pygame.K_w]:
            self.images = self.images_up
            self.update()
        if key[pygame.K_s]:
            self.images = self.images_down
            self.update()
        if key[pygame.K_a]:
            self.images = self.images_left
            self.update()
        if key[pygame.K_d]:
            self.images = self.images_right
            self.update()

    def update(self):
        '''This method iterates through the elements inside self.images and
        displays the next one each tick. For a slower animation, you may want to
        consider using a timer of some sort so it updates slower.'''
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]
        #time.sleep(self.timer)
