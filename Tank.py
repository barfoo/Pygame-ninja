import pygame


class Tank():

    def __init__(self, x, y):
        self.dx = 0
        self.dy = 0
        self.images = []
        self.images.append(pygame.image.load('images/tank.png'))
        self.images.append(pygame.image.load('images/tank2.png'))
        self.image = pygame.image.load('images/tank.png')
        self.rect = pygame.Rect((x, y, 32, 32))
        self.index = 0
