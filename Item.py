import pygame


class Item(object):

    def __init__(self, posy, posx):
        #walls.append(self)
        self.rect = pygame.Rect(posx, posy, 32, 32)
        self.image = pygame.image.load('images/Chris_tree.png')

    def draw(self, screen):
        #pygame.draw.rect(screen, (255, 200, 0), self.rect)
        screen.blit(self.image, self.rect)
