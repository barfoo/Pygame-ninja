#import os

import Player
import pygame
import Level
import BackGround
from pygame import *
# Initialise pygame
#os.environ["SDL_VIDEO_CENTERED"] = "1"
pygame.init()

pygame.mixer.music.set_volume(0.1)
# Set up the display
pygame.display.set_caption("Fucking Scrolling map!")
screen = pygame.display.set_mode((1024, 768))
clock = pygame.time.Clock()
#initialize some variables
myfont = pygame.font.SysFont("monospace", 25)
intro = True
finish = False
win = False
background = BackGround.BackGround()
#Intro game

intro_img = pygame.image.load('images/ninjaintro.png')
gameover_img = pygame.image.load('images/game_over.png')
youwin_img = pygame.image.load('images/youwin.png')
#pygame.mixer.music.load('sound/intro.mp3')
try:
    pygame.mixer.music.load('sound/music.mp3')
    pygame.mixer.music.play(1)
except:
    print("music error")
    pygame.mixer.stop()


while intro is True:
    screen.blit(intro_img, (0, 0, 0, 0))
    for e in pygame.event.get():
        if e.type == pygame.MOUSEBUTTONDOWN:
            intro = False
    pygame.display.flip()

#Begin of game
#Create level and walls
level = Level.Level(screen, 'map.txt')
# Create the player
player = Player.Player()
#shuriken = Shuriken.Shuriken(player.rect.x, player.rect.y)

shurikens = []
shoots = []
#draw first scene
level.drawEscene()
score = len(level.enemies)
mouse = pygame.mouse.get_pos()
running = True
#pygame.mixer.music.stop()

while running:

    if player.life < 0:
        running = False
    screen.fill((0, 0, 0))
    clock.tick(6000)
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
            running = False
        if e.type == pygame.MOUSEBUTTONDOWN:
            mouse = pygame.mouse.get_pos()
        if e.type == pygame.MOUSEBUTTONUP:
            if (len(shurikens) < 3):
                level.create_shuriken(player, shurikens, mouse)
                shuriken_sound = pygame.mixer.Sound("sound/Soft_Airy_Swish.ogg")
                shuriken_sound.set_volume(0.1)
                shuriken_sound.play()

    key = pygame.key.get_pressed()
    level.move(key, player, shurikens, mouse)
    score = level.update(shurikens, player, score, shoots)
    screen.blit(level.surface, (level.rect.x, level.rect.y, 800, 600))

    for shuriken in shurikens:
        screen.blit(shuriken.image,
                    (shuriken.rect.x + level.rect.x,
                       shuriken.rect.y + level.rect.y, 4, 4))
        shuriken.update()

    for shoot in shoots:
        screen.blit(shoot.image,
                    (shoot.rect.x + level.rect.x,
                       shoot.rect.y + level.rect.y, 4, 4))
        shoot.update()

    for enemy in level.enemies:
        screen.blit(enemy.image,
                 (enemy.rect.x + level.rect.x,
                      enemy.rect.y + level.rect.y, 32, 32))
        '''x = abs(abs(level.rect.x) - abs(enemy.rect.x))
        y = abs(abs(level.rect.y) - abs(enemy.rect.y))
        if x < 1000 and y < 800:'''
        enemy.update(level, player)

    screen.blit(player.image, player.rect)
    for item in level.items:
        screen.blit(item.image,
                    (item.rect.x + level.rect.x,
                       item.rect.y + level.rect.y, 4, 4))

    label = myfont.render("ENEMIES " + str(
        shuriken.rect.x) + " " + str(score), 1, (250, 250, 0))
    labe2 = myfont.render("LIFE " + str(player.life), 1, (250, 0, 250))
    screen.blit(label, (0, 10))
    screen.blit(labe2, (0, 35))
    pygame.display.flip()
    if score <= 0:
        running = False
        win = True

while finish is False:
    if win is True:
        screen.blit(youwin_img, (0, 0, 0, 0))
    if win is False:
        screen.blit(gameover_img, (0, 0, 0, 0))
    pygame.display.flip()
    for e in pygame.event.get():
        if e.type == pygame.MOUSEBUTTONDOWN:
            finish = True
